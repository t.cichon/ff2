const routers = [
    {
        path: '/',
        redirect: '/stadt/muenster',
        name: 'home',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/index.vue'], resolve),
    },
    {
        path: '/stadt/:cityId',
        name: 'city',
        meta: {
            title: 'Alle Wohnungen der letzten Tage'
        },
        component: (resolve) => require(['./views/results.vue'], resolve)
    },
    {
        path: '/stadt/:cityId/:apartmentId',
        name: 'expose',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/expose.vue'], resolve)
    }    
];
export default routers;
