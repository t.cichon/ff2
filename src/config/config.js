import Env from './env';

let config = {
    env: Env,
    apiUrl: 'http://localhost:3003/api',
    googleMapsApiKey: 'YOUR-GOOGLE-MAPS-API-KEY-HERE'
};
export default config;
