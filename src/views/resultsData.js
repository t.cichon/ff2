let data = {
    map: { 
        defaultCenter: {lat: 51.95800023, lng: 7.61970188},
        options: {
            center: {lat: 51.95800023, lng: 7.61970188},
            zoom: 13,
            zoomControl: true, 
            mapTypeControl: false, 
            streetViewControl: false, 
            fullscreenControl: false,
            styles: []
        }
    },
    filterCollapsed: false,
    mapVisible: true,
    apartments: [],
    pages: {
        total: 1,
        current: 1,
        size: 20
    },
    errors: [],
    openFilters: ['daysToLookBack'],
    filter: {
        rooms: {
            active: false,
            range: [2, 8],
            
        },
        size: {
            active: false,
            range: [50, 120]
        },
        price: {
            active: false,
            range: [400, 1200]
        },
        features: {
            main: {
                kitchen: {
                    label: 'Küche',
                    value: 'küche',
                    active: false
                },
                balcony: {
                    label: 'Balkon',
                    value: 'balkon',
                    active: false
                },
                terrace: {
                    label: 'Terrasse',
                    value: 'terrasse',
                    active: false
                },
                basement: {
                    label: 'Keller',
                    value: 'keller',
                    active: false
                },
                lift: {
                    label: 'Aufzug',
                    value: 'aufzug',
                    active: false
                },
                shower: {
                    label: 'Dusche',
                    value: 'dusche',
                    active: false
                },
                garage: {
                    label: 'Garage',
                    value: 'garage',
                    active: false
                },
                bathtub: {
                    label: 'Badewanne',
                    value: 'wanne',
                    active: false
                },
                guestToilet: {
                    label: 'Gäste-WC',
                    value: 'gäste-wc',
                    active: false
                },
                washroom: {
                    label: 'Waschraum',
                    value: 'waschraum',
                    active: false
                },
                floorHeating: {
                    label: 'Fußbodenheizung',
                    value: 'fußbodenheizung',
                    active: false
                },
                centralHeating: {
                    label: 'Zentralheizung',
                    value: 'zentralheizung',
                    active: false
                },  
                attic: {
                    label: 'Dachboden',
                    value: 'dachboden',
                    active: false
                }, 
                tiles: {
                    label: 'Fliesenboden',
                    value: 'fliesen',
                    active: false
                },
                parquet: {
                    label: 'Parkettboden',
                    value: 'parkett',
                    active: false
                }, 
                carpet: {
                    label: 'Teppichboden',
                    value: 'teppich',
                    active: false
                },
                laminate: {
                    label: 'Laminat',
                    value: 'laminat',
                    active: false
                }, 
                pvc: {
                    label: 'PVC / Linoleum',
                    value: 'pvc',
                    active: false
                },
                cableConnection: {
                    label: 'Kabelanschluss',
                    value: 'kabelanschluss',
                    active: false
                }
            },
            buildingState: {
                oldBuilding: {
                    label: 'Altbau',
                    value: 'altbau',
                    active: false
                }, 
                newBuilding: {
                    label: 'Neubau',
                    value: 'neubau',
                    active: false
                },
                renovated: {
                    label: 'Renoviert',
                    value: 'renoviert',
                    active: false
                },  
                furnished: {
                    label: 'Möbliert',
                    value: 'möbliert',
                    active: false
                }
            },
            accessibility: {
                barrierFree: {
                    label: 'Barrierefrei',
                    value: 'barrierefrei',
                    active: false
                },
                wheelchair: {
                    label: 'Rollstuhlgerecht',
                    value: 'rollstuhl',
                    active: false
                }, 
                stepless: {
                    label: 'Stufenloser Zugang',
                    value: 'stufenlos',
                    active: false
                },
            },
            pets: {
                label: 'Angabe zu Haustieren',
                value: 'haustiere',
                active: false
            },
            flatSharingSuitable: {
                label: 'WG-geeignet',
                value: 'WG-geeignet',
                active: false
            }
        }
    },
    lookBackOptions: {
        1: {
            value: 1,
            label: 'Wohnungsanzeigen der letzten 24 Stunden'
        },
        2: {
            value: 2,
            label: 'Wohnungsanzeigen der letzten 2 Tage'
        },
        3: {
            value: 3,
            label: 'Wohnungsanzeigen der letzten 3 Tage'
        },
        4: {
            value: 4,
            label: 'Wohnungsanzeigen der letzten 4 Tage'
        },
        5: {
            value: 5,
            label: 'Wohnungsanzeigen der letzten 5 Tage'
        },
        6: {
            value: 6,
            label: 'Wohnungsanzeigen der letzten 6 Tage'
        },
        7: {
            value: 7,
            label: 'Wohnungsanzeigen der letzten Woche'
        },
        14: {
            value: 14,
            label: 'Wohnungsanzeigen der letzten 2 Wochen'
        },
        31: {
            value: 31,
            label: 'Wohnungsanzeigen des letzten Monats'
        }
    },
    daysToLookBack: 3,
    cities: []
};

module.exports = data;
